

# **P0 Javalin app**

## Routes

#### POST         /register
allow new users to register
#### POST         /login
allow existing users to login
#### GET        /users
return list of all users
#### GET         /users/1
return user of id 1
#### DELETE     /users/1
delete user with id 1
#### PUT         /users/1
update user with id 1

#### GET        /items
get all items of current user
#### POST        /items
add item for current user
#### GET         /items/1
get item with id 1
#### DELETE     /items/1
delete item with id 1
#### PUT         /items/1
update item with id 1


