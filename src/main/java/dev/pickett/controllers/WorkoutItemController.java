package dev.pickett.controllers;


import dev.pickett.models.WorkoutItem;
import dev.pickett.services.WorkoutItemService;
import io.javalin.http.BadGatewayResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

public class WorkoutItemController {

        Logger logger= LoggerFactory.getLogger(WorkoutItemController.class);
        private WorkoutItemService service = new WorkoutItemService();

        public void handleGetAllItems(Context ctx){

                if(ctx.formParam("query")==null) {

                        if(ctx.header("userID")!=null){
                                ctx.json(service.getAll(Integer.parseInt(ctx.header("userID"))));
                        }else{
                                ctx.json(service.getAll());
                        }
                }else{
                        if(ctx.header("userID")!=null){
                                ctx.json(service.getByName(ctx.formParam("query"),Integer.parseInt(ctx.header("userID"))));
                        }else{
                                ctx.json(service.getByName(ctx.formParam("query")));
                        }


                }
        }

        public void handleGetItemById(Context ctx){

                int id = Integer.parseInt(ctx.pathParam("id"));
                WorkoutItem d = service.getById(id);
                if(d==null){
                        logger.warn("no item present with id: {}",id);
                        throw new NotFoundResponse();
                }else{
                        ctx.json(d);
                }
        }
        public void handlePostItem(Context ctx){

                int id = Integer.parseInt(ctx.formParam("id"));
                double calories = parseWithDefault(ctx.formParam("calories"),0.0);
                String name = ctx.formParam("name");
                int sets = parseWithDefault(ctx.formParam("sets"),0);
                int reps = parseWithDefault(ctx.formParam("reps"),0);
                int userID = parseWithDefault(ctx.formParam("userID"),0);
                LocalDate date = LocalDate.parse(ctx.formParam("date"));

                service.add(new WorkoutItem(id,calories,name,sets,reps,userID,date));//201 created
                ctx.status(201);
        }
        public void handlePutItem(Context ctx){

                //WILL NEED TO ENFORCE NONE NULL numbers (default submit of 0 or 0.0)
                int id = Integer.parseInt(ctx.formParam("id"));
                double calories = parseWithDefault(ctx.formParam("calories"),0.0);
                String name = ctx.formParam("name");
                int sets = parseWithDefault(ctx.formParam("sets"),0);
                int reps = parseWithDefault(ctx.formParam("reps"),0);
                int userID = parseWithDefault(ctx.formParam("userID"),0);
                LocalDate date = LocalDate.parse(ctx.formParam("date"));
                WorkoutItem updatedItem = service.update(new WorkoutItem(id,calories,name,sets,reps,userID,date));
                ctx.status(201);
                ctx.json(updatedItem);

        }
        public void handleDeleteItem(Context ctx){
                String idString = ctx.pathParam("id");
                if(idString.matches("^\\d+$")){
                        int id = Integer.parseInt(idString);
                        service.delete(id);
                }else{
                        throw new BadGatewayResponse("Input \""+idString+"\" cannot be parsed to an int");
                }

        }
        public static int parseWithDefault(String s, int defaultVal) {
                if(s==null){
                        return 0;
                }
                return s.matches("-?\\d+") ? Integer.parseInt(s) : defaultVal;
        }
        public static Double parseWithDefault(String s, double defaultVal) {
                if(s==null){
                        return 0.0;
                }
                return s.matches("-?\\d+") ? Double.parseDouble(s) : defaultVal;
        }



}
