package dev.pickett.controllers;

import dev.pickett.util.ConnectionUtil;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthController {

    Logger logger = LoggerFactory.getLogger(AuthController.class);
    private Connection connection = null;
    private PreparedStatement stmt = null;

    public void authenticateLogin(Context ctx){
        // getting params from what would be a form submission (content-type: application/x-www-form-urlencoded)

        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");

        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("SELECT * FROM users WHERE username=? AND pass=?");
            stmt.setString(1,user);
            stmt.setString(2,pass);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                ctx.header("Authorization", "admin-auth-token");
                ctx.header("UserID",String.valueOf(rs.getInt("id")));
                ctx.status(200);
                logger.info("{} successful login", user);
                return;
            }


        }catch(SQLException e){
            logger.error("SQl exception when attempting to log in for {}",user);
        }finally{
            closeResources();
        }
        logger.warn("{} failed login",user);
        throw new UnauthorizedResponse("Credentials were incorrect");

    }
    public void authorizeToken(Context ctx){
        String authHeader =ctx.header("Authorization");
        if(authHeader!=null && authHeader.equals("admin-auth-token")){
            logger.info("request is authorized, proceeding to handler method");
        }else{
            logger.warn("improper authorization");
            throw new UnauthorizedResponse();
        }
    }
    private void logExceptions(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }

    private void closeResources() {
        try{
            if(stmt !=null){
                stmt.close();
            }
        } catch (SQLException e) {
            logExceptions(e);
        }
        try{
            if(connection != null){
                connection.close();
            }
        }catch(SQLException e){
            logExceptions(e);
        }
    }
}
