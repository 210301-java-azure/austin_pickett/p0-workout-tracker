package dev.pickett.models;

import java.io.Serializable;
import java.time.LocalDate;

public class WorkoutItem implements Serializable {

    private int id;
    private double calories;
    private String name;
    private int sets;
    private int reps;
    private int userID;
    private LocalDate date;

    public WorkoutItem(){

    }

    public WorkoutItem(int id, double calories, String name, int sets, int reps, int userID, LocalDate date) {
        this.id = id;
        this.calories = calories;
        this.name = name;
        this.sets = sets;
        this.reps = reps;
        this.userID = userID;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getReps() {
        return reps;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "WorkoutItem{" +
                "id=" + id +
                ", calories=" + calories +
                ", name='" + name + '\'' +
                '}';
    }


}
