package dev.pickett.data;

import dev.pickett.models.WorkoutItem;

import java.util.List;

public interface WorkoutItemDAO {

    List<WorkoutItem> getAllWorkoutItems();
    List<WorkoutItem> getAllWorkoutItems(int userID);
    WorkoutItem getWorkoutItemById(int id);
    List<WorkoutItem> getWorkoutItemsByName(String name);
    List<WorkoutItem> getWorkoutItemsByName(String name,int userID);
    WorkoutItem updateWorkoutItem(WorkoutItem item);
    WorkoutItem addNewWorkoutItem(WorkoutItem item);
    boolean deleteWorkoutItem(int id);

}
