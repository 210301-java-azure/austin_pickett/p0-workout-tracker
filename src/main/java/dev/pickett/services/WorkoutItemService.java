package dev.pickett.services;

import dev.pickett.data.WorkoutItemDAO;
import dev.pickett.data.WorkoutItemDAOImpl;
import dev.pickett.models.WorkoutItem;

import java.util.List;

public class WorkoutItemService {

    private final WorkoutItemDAO dao = new WorkoutItemDAOImpl();

    public List<WorkoutItem> getAll(int userID){return dao.getAllWorkoutItems(userID);}
    public List<WorkoutItem> getAll(){return dao.getAllWorkoutItems();}
    public WorkoutItem getById(int id){return dao.getWorkoutItemById(id);}

    public List<WorkoutItem> getByName(String name){return dao.getWorkoutItemsByName(name);}
    public List<WorkoutItem> getByName(String name, int userID){return dao.getWorkoutItemsByName(name,userID);}

    public WorkoutItem add(WorkoutItem item){return dao.addNewWorkoutItem(item);}

    public WorkoutItem update(WorkoutItem item){return dao.updateWorkoutItem(item);}

    public boolean delete(int id){return dao.deleteWorkoutItem(id);}


}
