package dev.pickett.services;

import dev.pickett.data.UserDAO;
import dev.pickett.data.UserDAOImpl;
import dev.pickett.models.User;

import java.util.List;

public class UserService {
    private final UserDAO dao = new UserDAOImpl();

    public List<User> getAll(){return dao.getAllUsers();}

    public User getById(int id){return dao.getUserById(id);}

    public List<User> search(String query){return dao.searchUsers(query);}

    public User add(User user){return dao.addUser(user);}

    public User update(User user){return dao.updateUser(user);}

    public boolean delete(int id){return dao.deleteUser(id);}

}
